@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Informações do Funcionário</div>
                  <div class="panel-body">
                    <table class="table table-striped">
                      <tbody>
                          <tr>
                            <th>ID:</th>
                            <td>{{ $employee->id }}</td>
                          </tr>
                          <tr>
                            <th>Nome:</th>
                            <td>{{ $employee->name }}</td>
                          </tr>
                          <tr>
                            <th>Email:</th>
                            <td>{{ $employee->email }}</td>
                          </tr>
                      </tbody>
                    </table>
                    <a href="{{ route('employees.index') }}" class="btn btn-primary"><< Voltar</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
