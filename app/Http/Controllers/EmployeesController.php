<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Session;

class EmployeesController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        $employees = Employee::all()->sortByDesc('id');
        return view('employees.index')->withEmployees($employees);
    }

    public function create()
    {
        return view('employees.create');
    }

    public function store(Request $request)
    {
        $request->validate([
          'employeeName' => 'required',
          'employeeEmail' => 'required|email',
        ]);

        $employee = new Employee();
        $employee->name = $request->employeeName;
        $employee->email = $request->employeeEmail;

        if ($employee->save()) {
          Session::flash('success', 'O funcionário foi cadastrado com sucesso!');
        } else {
          Session::flash('failed', 'Ocorreu algum erro ao tentar cadastrar o funcionário, tente novamente.');
        }

        return redirect()->route('employees.index');
    }

    public function show($id)
    {
        $employee = Employee::find($id);
        return view('employees.show')->withEmployee($employee);
    }

    public function edit($id)
    {
        $employee = Employee::find($id);
        return view('employees.edit')->withEmployee($employee);
    }

    public function update(Request $request, $id)
    {
      $request->validate([
        'employeeName' => 'required',
        'employeeEmail' => 'required|email',
      ]);

      $employee = Employee::find($id);
      $employee->name = $request->employeeName;
      $employee->email = $request->employeeEmail;

      if ($employee->save()) {
        Session::flash('success', 'O funcionário foi editado com sucesso!');
      } else {
        Session::flash('failed', 'Ocorreu algum erro ao tentar editar o funcionário, tente novamente.');
      }

        return redirect()->route('employees.index');
    }

    public function destroy($id)
    {
        $employee = Employee::find($id);

        if ($employee->delete()) {
          Session::flash('success', 'O funcionário foi deletado com sucesso!');
        }

        return redirect()->route('employees.index');
    }
}
